<?php

namespace Tests\Feature;

use App\Models\Game;
use App\Models\Guess;
use App\Models\Word;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use Wordle\Game\Actions\MatchGuessAction;

class ViewGameStateTest extends TestCase
{
    use RefreshDatabase;

    public function test_viewing_a_game(): void
    {
        $word = Word::factory()->create(['word' => 'baker']);
        $game = Game::factory()->word($word)->create();

        $this
            ->get("/api/game/{$game->id}")
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', true)
                ->has('payload.game', fn (AssertableJson $json) => $json
                    ->has('id')
                    ->where('finished', false)
                    ->where('attempts', 0)
                    ->where('attempts_left', config('wordle.max_attempts'))
                    ->where('won', false)
                    ->where('guesses', [])
                    ->has('word', fn (AssertableJson $json) => $json
                        ->where('id', $word->id)
                        ->where('word', $word->word)
                    )
                )
                ->where('error', null)
            );
    }

    /**
     * @return void data_guesses_left
     */
    public function test_guesses_left(): void
    {
        $word = Word::factory()->create(['word' => 'baker']);
        $game = Game::factory()->word($word)->create();

        collect(range(1, config('wordle.max_attempts') + 1))
            ->each(function ($attemptsCount) use ($game) {
                Guess::factory()->game($game)->create(['word' => 'abcde']);

                $this
                    ->get("/api/game/{$game->id}")
                    ->assertStatus(200)
                    ->assertJson(fn (AssertableJson $json) => $json
                        ->where('success', true)
                        ->has('payload.game', fn (AssertableJson $json) => $json
                            ->has('id')
                            ->where('attempts', $attemptsCount)
                            ->where(
                                'attempts_left',
                                max(config('wordle.max_attempts') - $game->guesses()->count(), 0)
                            )
                            ->etc()
                        )
                        ->where('error', null)
                    );
            });
    }
    /**
     * @return void data_guesses_left
     */
    public function test_show_guesses(): void
    {
        $word = Word::factory()->create(['word' => 'baker']);
        $game = Game::factory()->word($word)->create();

        $guessOne = Guess::factory()->game($game)->create(['word' => 'abcde']);
        $guessTwo = Guess::factory()->game($game)->create(['word' => 'abcdf']);

        $expectedGuesses = collect([$guessOne, $guessTwo])
            ->map(fn (Guess $guess) =>
                [
                    'id' => $guess->id,
                    'guess' => $guess->word,
                    'correct' => $guess->correct,
                    'valid' => $guess->valid,
                    'match' => resolve(MatchGuessAction::class)->execute($word->word, $guess->word)
                ])
            ->toArray();

        $this
            ->get("/api/game/{$game->id}")
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', true)
                ->has('payload.game', fn (AssertableJson $json) => $json
                    ->has('id')
                    ->where('guesses', $expectedGuesses)
                    ->etc()
                )
                ->where('error', null)
            );
    }
}
