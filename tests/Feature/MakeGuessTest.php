<?php

namespace Tests\Feature;

use App\Models\Game;
use App\Models\Guess;
use App\Models\Word;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class MakeGuessTest extends TestCase
{
    use RefreshDatabase;

    public function test_making_a_correct_guess(): void
    {
        $word = Word::factory()->create(['word' => 'baker']);
        $game = Game::factory()->word($word)->create();

        $guess = 'baker';

        $this
            ->post(route('game.guess', $game), [
                'guess' => $guess,
            ])
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', true)
                ->has('payload.guess', fn (AssertableJson $json) => $json
                    ->has('id')
                    ->where('guess', $guess)
                    ->where('correct', true)
                    ->where('valid', true)
                    ->where('match', 'ccccc')
                )
                ->where('error', null)
            );

        $this
            ->get(route('game.view', $game))
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('payload.game.finished', true)
                ->where('payload.game.won', true)
                ->etc()
            )
        ;
    }

    public function test_making_an_incorrect_guess(): void
    {
        $word = Word::factory()->create(['word' => 'baker']);
        $game = Game::factory()->word($word)->create();

        $guess = 'raked';

        $this
            ->post(route('game.guess', $game), [
                'guess' => $guess,
            ])
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', true)
                ->has('payload.guess', fn (AssertableJson $json) => $json
                    ->has('id')
                    ->where('guess', $guess)
                    ->where('correct', false)
                    ->where('valid', true)
                    ->where('match', 'acccx')
                )
                ->where('error', null)
            );
    }

    public function test_making_an_invalid_guess(): void
    {
        $word = Word::factory()->create(['word' => 'baker']);
        $game = Game::factory()->word($word)->create();

        $guess = 'invalid';

        $this
            ->post(route('game.guess', $game), [
                'guess' => $guess,
            ])
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', true)
                ->has('payload.guess', fn (AssertableJson $json) => $json
                    ->has('id')
                    ->where('guess', $guess)
                    ->where('correct', false)
                    ->where('valid', false)
                    ->where('match', null)
                )
                ->where('error', null)
            );
    }

    public function test_making_a_guess_when_the_game_is_finished(): void
    {
        $guess = 'baker';
        $word = Word::factory()->create(['word' => $guess]);
        $game = Game::factory()->word($word)->create();
        $guess = Guess::factory()->game($game)->create(['word' => $guess]);

        $this
            ->post(route('game.guess', $game), [
                'guess' => $guess,
            ])
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', false)
                ->where('payload', null)
                ->where('error', 'GAME_ENDED')
            );

        $this
            ->get(route('game.view', $game))
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('payload.game.finished', true)
                ->where('payload.game.won', true)
                ->etc()
            )
        ;
    }

    public function test_making_too_many_votes(): void
    {
        $guess = 'baker';
        $word = Word::factory()->create(['word' => $guess]);
        $game = Game::factory()->word($word)->create();
        $guess = Guess::factory()->game($game)->count(config('wordle.max_attempts'))->create(['word' => 'abcde']);

        $this
            ->post(route('game.guess', $game), [
                'guess' => $guess,
            ])
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', false)
                ->where('payload', null)
                ->where('error', 'GAME_ENDED')
            );

        $this
            ->get(route('game.view', $game))
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('payload.game.finished', true)
                ->etc()
            );
    }

}
