<?php

namespace Tests\Feature;

use App\Models\Game;
use App\Models\Word;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateNewGameTest extends TestCase
{
    use RefreshDatabase;

    public function test_creating_a_new_game(): void
    {
        $word = Word::factory()->create(['word' => 'baker']);

        $this->assertDatabaseEmpty(Game::class);

        $this
            ->get('/api/game')
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json
                ->where('success', true)
                ->has('payload.game', fn (AssertableJson $json) => $json
                    ->has('id')
                    ->where('finished', false)
                    ->where('attempts', 0)
                    ->where('attempts_left', config('wordle.max_attempts'))
                    ->where('won', false)
                    ->where('guesses', [])
                    ->has('word', fn (AssertableJson $json) => $json
                        ->where('id', $word->id)
                        ->where('word', $word->word)
                    )
                )
                ->where('error', null)
            );
    }
}
