<?php

namespace Tests\Unit\Game\Actions;

use PHPUnit\Framework\TestCase;
use Wordle\Game\Actions\ValidateGuessAction;

class ValidateGuessActionTest extends TestCase
{
    /**
     * @dataProvider guessesData
     */
    public function test_if_guesses_are_valid(string $guess, bool $expected): void
    {
        /** @var ValidateGuessAction $action */
        $action = resolve(ValidateGuessAction::class);

        $this->assertEquals(
            $expected,
            $action->execute($guess)
        );
    }

    public static function guessesData(): array
    {
        return [
            ['guess' => 'abcde', 'expected' => true,],
            ['guess' => 'abce', 'expected' => false,],
            ['guess' => 'abcdde', 'expected' => false,],
            ['guess' => 'abc#e', 'expected' => false,],
        ];
    }
}
