<?php

namespace Tests\Unit\Game\Actions;

use PHPUnit\Framework\TestCase;
use Wordle\Game\Actions\MatchGuessAction;
use Wordle\Game\Exceptions\IncompatibleStringSizesException;

class MatchGuessActionTest extends TestCase
{
    /**
     * @dataProvider data_matching
     */
    public function test_matching(string $word, string $guess, string $expectedMatch): void
    {
        $this->assertEquals(
            $expectedMatch,
            resolve(MatchGuessAction::class)->execute($word, $guess)
        );
    }

    public static function data_matching(): array
    {
        return [
            [
                'word' => 'baker',
                'guess' => 'baker',
                'match' => 'ccccc',
            ],
            [
                'word' => 'baker',
                'guess' => 'armed',
                'match' => 'aaxcx',
            ],
        ];
    }


    /**
     * @dataProvider data_incompatible
     */
    public function test_exception_is_thrown_if_strings_are_incompatible(string $word, string $guess): void
    {
        $this->expectException(IncompatibleStringSizesException::class);

        resolve(MatchGuessAction::class)->execute($word, $guess);
    }

    public static function data_incompatible(): array
    {
        return [
            [
                'word' => 'match',
                'guess' => 'matc',
            ],
        ];
    }
}
