<?php

use App\Http\Controllers\Api\GameController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::name('game.')
    ->prefix('game')
    ->group(function() {
        Route::get('/', [GameController::class, 'new'])->name('create');
        Route::get('/{game}', [GameController::class, 'read'])->name('view');
        Route::post('/{game}/guess', [GameController::class, 'guess'])->name('guess');
    });
