# Wordle

A wordle game 

![A Screenshot of the Wordle game in this repo](https://codeberg.org/myrmidex/wordle/raw/branch/main/resources/assets/screenshot.png)

## Installation

First clone the repo. Set up .env, install composer and npm packages. This should be enough to get the Sail containers
up and running. File an issue on Codeberg if you encounter problems.

## Usage

In the cloned directory, run Sail:

```
./vendor/bin/sail up
```

Then in a browser go to `http://localhost` to play the game

## Tests
To run the tests, use
```
./vendor/bin/sail test
```
