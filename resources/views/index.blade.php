<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, width=device-width" />
    <title>Laravel</title>

    @vite(['resources/css/app.css'])
</head>

<body>
    <div id="app"></div>

    @viteReactRefresh
    @vite(['resources/js/main.jsx'])
</body>

</html>
