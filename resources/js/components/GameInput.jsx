import * as React from 'react';
import {FormControl, Stack, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import {useState} from "react";

export default function GameInput({ onGuessSubmit, setError }) {
    const [guess, setGuess] = useState('');

    const handleGuessChange = (e) => {
        setError(null);
        setGuess(e.target.value.toLowerCase());
    };

    const handleGuessSubmit = (e) => {
        e.preventDefault();

        if (validateGuess() === false) {
            return setError('Invalid Guess');
        }

        setGuess('');
        onGuessSubmit(guess);
    }

    function validateGuess() {
        if (guess.length !== 5) return false;

        if (guess !== guess.replace(/[^A-Z]+/gi, '')) return false;

        return true;
    }

    return (
        <div style={{paddingTop: '20px'}}>
            <form onSubmit={handleGuessSubmit}>
                <FormControl>

                    <Stack
                        sx={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '100%',
                            padding: '30px',
                        }}
                        spacing={2}
                    >
                        <TextField
                            autoComplete="off"
                            id="guessInput"
                            label="Guess"
                            name="guess"
                            onChange={handleGuessChange}
                            value={guess}
                            variant="outlined"
                            focused
                        />
                        <Button
                            variant="contained"
                            type="submit"
                        >Guess</Button>
                    </Stack>
                </FormControl>
            </form>
        </div>
    );
}
