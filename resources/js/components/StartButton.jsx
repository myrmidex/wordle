import * as React from 'react';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import {useState} from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom";

export default function StartButton({ title = 'Start Game' }) {
    const navigate = useNavigate();
    const [isLoading, setIsLoading] = useState(false);

    function handleClick() {
        setIsLoading(true);
        axios
            .get(`/api/game/`)
            .then((response) => {
                setIsLoading(false);
                navigate('/game/' + response.data.payload.game.id);
                window.location.reload(true);
            })
    }


    return (
        <Button variant="contained" loading={isLoading.toString()} startIcon={<SendIcon />} onClick={handleClick}>{title}</Button>
    );
}
