import {Card, CardContent} from "@mui/material";

function PageCard({ children }) {
    return (
        <Card variant="outlined" style={{marginTop: '200px' }}>
            <CardContent style={{textAlign: 'center', justifyContent: 'center', alignItems: 'center'}}>
                {children}
            </CardContent>
        </Card>
    );
}

export default PageCard;
