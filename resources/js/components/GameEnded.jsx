import * as React from "react";
import {Alert, Stack} from "@mui/material";
import StartButton from "./StartButton.jsx";

function GameEnded({won, word}) {

    const severity = won ? 'success' : 'error';
    const color = won ? 'success' : 'error';

    return (
        <Stack
            sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                padding: '30px',
            }}

            spacing={2}
        >
            <Alert variant="standard" severity={severity} style={{width: '200px'}}>
                Game Over - You { won ? 'Won' : 'Lost' }.
                <br/>
                <br/>
                The word was:
                <br/>
                <strong style={{fontSize: '25px'}}>{word}</strong>
            </Alert>
            <StartButton title="Play again" />
        </Stack>
    );
}

export default GameEnded;
