import * as React from 'react';
import {Card, CardContent, Grid} from "@mui/material";

export default function LetterBox({ letter, matchCode }) {
    function getColor() {
        switch (matchCode) {
            case 'c':
                return '#6ca965';
            case 'a':
                return '#c8b653';
            case 'x':
                return '#787c7f';
            default:
                return '#ffffff';

        }
    }

    return (
        <Card variant="outlined">
            <CardContent
                style={{
                    textAlign: 'center',
                    backgroundColor: getColor(),
                    height: '12px',
                    paddingTop: '12px',
                    paddingLeft: '15px',
                }}
            >
                <span className="letterbox" style={{padding: '0'}}>
                    { letter ? letter.toUpperCase() : '' }
                </span>
            </CardContent>
        </Card>
    );
}
