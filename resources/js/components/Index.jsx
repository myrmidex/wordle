import * as React from 'react';
import StartButton from "./StartButton.jsx";
import PageCard from "./PageCard.jsx";

export default function Index() {
    return (
        <PageCard>
            <StartButton />
        </PageCard>
    );
}
