import * as React from 'react';
import WordsGridRow from "./WordsGridRow.jsx";
import {Box} from "@mui/material";

export default function WordsGrid({ guesses = null }) {
    const ROWS = 6;

    const randNum = Math.random();

    return (
        <Box id="words-grid">
            {
                [...Array(ROWS).keys()]
                    .map(number => {
                        let guess = null;
                        if (guesses) guess = guesses[number];

                        return <WordsGridRow key={randNum + ':' + number} guess={guess} number={number} />;
                    })
            }
        </Box>
    );
}
