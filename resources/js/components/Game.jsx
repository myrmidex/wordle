import * as React from 'react';
import {Alert, Box} from "@mui/material";
import {useEffect, useState} from "react";
import { useParams } from "react-router-dom";
import GameField from "./GameField.jsx";
import GameInput from "./GameInput.jsx";
import {addGuess, getGame} from "../services/game.js";
import GameEnded from "./GameEnded.jsx";
import PageCard from "./PageCard.jsx";
import WordsGrid from "./WordsGrid.jsx";
import PageLoading from "./PageLoading.jsx";

function Game() {
    const [isLoading, setIsLoading] = useState(false);
    const [game, setGame] = useState({});
    const [error, setError] = useState(null);

    const { gameId } = useParams();

    useEffect(() => {
        setIsLoading(true)
        loadGame()
            .finally(() => setIsLoading(false))
    }, []);

    let loadGame = () => {
        return getGame(gameId)
            .then((data) => setGame(data.payload.game))
    }

    let handleGuessSubmit = async (guess) => {
        setError(null);

        await addGuess(gameId, guess)
            .then((res) => {
                if (res.success === true) {
                    loadGame();
                } else {
                    setError(res.error);
                }
            })
        ;
    }

    if (!game || isLoading) return <PageLoading />;

    return (
        <PageCard>
            <WordsGrid guesses={game.guesses} />

            {
                game.finished
                    ? <GameEnded won={game.won} word={game.word.word} />
                    : <GameInput onGuessSubmit={handleGuessSubmit} setError={setError} />
            }

            {
                error && (
                    <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Alert color="error" sx={{width: '200px', marginLeft: '60px'}}>
                            {error}
                        </Alert>
                    </Box>
                )
            }
        </PageCard>
    );
}

export default Game
