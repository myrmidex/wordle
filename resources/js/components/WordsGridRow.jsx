import * as React from 'react';
import {Grid} from "@mui/material";
import LetterBox from "./LetterBox.jsx";

export default function WordsGridRow({ guess = null, number }) {
    return (
        <Grid container spacing={1} key={number} style={{marginTop: '0px'}}>
            <Grid item xs={1}></Grid>
            {
                [...Array(5).keys()]
                    .map(charNumber => {
                        let char, match = null;

                        if (guess) {
                            char = guess.guess.split('')[charNumber];
                            match = guess.match.split('')[charNumber];
                        }

                        return (
                            <Grid item xs={2} key={charNumber}>
                                <LetterBox letter={char} matchCode={match} />
                            </Grid>
                        );
                    })
            }

            <Grid item xs={1}></Grid>
        </Grid>
    );
}
