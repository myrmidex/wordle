import * as React from 'react';
import {Grid} from "@mui/material";
import WordsGridRow from "./WordsGridRow.jsx";

export default function GameField({ game }) {
    return (
        <Grid container spacing={2}>
            {
                game.guesses
                    ? game.guesses.map(guess => (
                        <WordsGridRow guess={guess} key={guess.id} />
                    ))
                    : <div>Loading</div>
            }
        </Grid>
    );
}
