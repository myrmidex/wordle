import * as React from "react";
import PageCard from "./PageCard.jsx";

function PageLoading() {
    return (
        <PageCard>
            Loading...
        </PageCard>
    );
}

export default PageLoading;
