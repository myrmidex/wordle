import * as ReactDOM from "react-dom/client";
import "./index.css";
import {routes} from "./routes.jsx";

ReactDOM
    .createRoot(document.getElementById("app"))
    .render(routes);
