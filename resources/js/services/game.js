const basePath = '/api/game';

export const getGame = (gameId) => fetch(`${basePath}/${gameId}`)
    .then((data) => data.json());

export const addGuess = (gameId, guess) => fetch(`${basePath}/${gameId}/guess`, {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ guess })
}).then((data) => data.json());
