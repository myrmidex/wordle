import {BrowserRouter, Route, Routes} from "react-router-dom";
import Index from "./components/Index.jsx";
import * as React from "react";
import Game from "./components/Game.jsx";

export const routes = (
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<Index />} />
            <Route path="/game/:gameId" element={<Game />} />
        </Routes>
    </BrowserRouter>
);
