<?php

namespace Wordle\Helpers;

use Illuminate\Http\JsonResponse;

class JsonPresenter
{
    public function success(array $payload = []): JsonResponse
    {
        return $this->present(true, $payload);
    }

    public function error(?string $error = null): JsonResponse
    {
        return $this->present(false, null, $error);
    }

    private function present(bool $success, ?array $payload, ?string $error = null): JsonResponse
    {
        return response()
            ->json([
                'success' => $success,
                'payload' => $payload,
                'error' => $error,
            ]);
    }
}
