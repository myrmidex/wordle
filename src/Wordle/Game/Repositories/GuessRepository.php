<?php

namespace Wordle\Game\Repositories;

use App\Exceptions\GameEndedException;
use App\Models\Game;
use App\Models\Guess;

class GuessRepository
{
    /**
     * @throws GameEndedException
     */
    public function create(Game $game, string $guess): Guess
    {
        if ($game->finished) {
            throw new GameEndedException();
        }

        return Guess::create([
            'game_id' => $game->id,
            'word' => $guess,
        ]);
    }
}
