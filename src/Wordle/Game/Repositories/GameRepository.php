<?php

namespace Wordle\Game\Repositories;

use App\Models\Game;
use App\Models\Word;

class GameRepository
{
    public function create(): Game
    {
        return Game::create([
            'word_id' => Word::inRandomOrder()->first()->id,
        ]);
    }
}
