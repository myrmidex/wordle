<?php

namespace Wordle\Game\Actions;

class ValidateGuessAction
{
    public function execute(string $guess): bool
    {
        $chars = preg_replace('/[^a-z]/i', '', $guess);

        if (strlen($chars) !== strlen($guess)) return false;

        if (strlen($guess) !== 5) return false;

        return true;
    }
}
