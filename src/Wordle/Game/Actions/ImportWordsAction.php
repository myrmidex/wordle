<?php

namespace Wordle\Game\Actions;

use App\Models\Word;
use Wordle\Game\Exceptions\WordlistProviderUnreachableException;

class ImportWordsAction
{
    protected const WORDLIST_SOURCE = 'https://gist.githubusercontent.com/dracos/dd0668f281e685bad51479e5acaadb93/raw/6bfa15d263d6d5b63840a8e5b64e04b382fdb079/valid-wordle-words.txt';

    /**
     * @throws WordlistProviderUnreachableException
     */
    public function execute(): void
    {
        collect($this->fetchWordList())
            ->map(function (string $word) {
                Word::create([
                    'word' => $word
                ]);
            });
    }

    /**
     * @throws WordlistProviderUnreachableException
     */
    private function fetchWordList(): array
    {
        $wordlistContent = file_get_contents(self::WORDLIST_SOURCE);

        if ($wordlistContent === false) {
            throw new WordlistProviderUnreachableException();
        }

        return explode("\n", $wordlistContent);
    }

}
