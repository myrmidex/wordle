<?php

namespace Wordle\Game\Actions;

use Wordle\Game\Exceptions\IncompatibleStringSizesException;

class MatchGuessAction
{
    public const MATCH_CORRECT = 'c';
    public const MATCH_ALMOST = 'a';
    public const MATCH_FAILED = 'x';

    /**
     * @throws IncompatibleStringSizesException
     */
    public function execute(string $word, string $guess): string
    {
        if (strlen($word) !== strlen($guess)) {
            throw new IncompatibleStringSizesException();
        }

        $wordArr = str_split($word);

        return collect(str_split($guess))
            ->map(function (string $guessChar, int $index) use ($wordArr) {
                $wordChar = $wordArr[$index];
                if ($guessChar === $wordChar) return self::MATCH_CORRECT;
                if (in_array($guessChar, $wordArr)) return self::MATCH_ALMOST;
                return self::MATCH_FAILED;
            })
            ->join('');
    }
}
