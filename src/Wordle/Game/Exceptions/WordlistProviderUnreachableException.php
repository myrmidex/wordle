<?php

namespace Wordle\Game\Exceptions;

use Exception;

class WordlistProviderUnreachableException extends Exception
{
    public $message = 'WORDLIST_PROVIDER_UNREACHABLE';
}
