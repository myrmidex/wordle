<?php

namespace Wordle\Game\Exceptions;

use Exception;

class IncompatibleStringSizesException extends Exception
{
    public $message = 'INCOMPATIBLE_STRING_SIZES';
}
