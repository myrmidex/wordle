<?php

namespace Wordle\Game\Transformers;

use App\Models\Guess;

class GuessTransformer
{
    public function transform(Guess $guess): array
    {
        return [
            'id' => $guess->id,
            'guess' => $guess->word,
            'correct' => $guess->correct,
            'valid' => $guess->valid,
            'match' => $guess->match,
        ];
    }
}
