<?php

namespace Wordle\Game\Transformers;

use App\Models\Game;
use App\Models\Guess;

class GameTransformer
{
    public function transform(Game $game): array
    {
        $attemptDiff = config('wordle.max_attempts') - $game->guesses()->count();

        return [
            'id' => $game->id,
            'word' => resolve(WordTransformer::class)->transform($game->word),
            'finished' => $game->finished,
            'attempts' => $game->guesses()->count(),
            'attempts_left' => max($attemptDiff, 0),
            'guesses' => $this->resolveGuesses($game),
            'won' => $game->won,
        ];
    }

    private function resolveGuesses(Game $game): array
    {
        $guessTransformer = resolve(GuessTransformer::class);
        return $game->guesses->map(function(Guess $guess) use ($guessTransformer) {
            return $guessTransformer->transform($guess);
        })->toArray();
    }
}
