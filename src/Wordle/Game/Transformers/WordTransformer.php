<?php

namespace Wordle\Game\Transformers;

use App\Models\Word;

class WordTransformer
{
    public function transform(Word $word): array
    {
        return [
            'id' => $word->id,
            'word' => $word->word,
        ];
    }
}
