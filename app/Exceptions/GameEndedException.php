<?php

namespace App\Exceptions;

use Exception;

class GameEndedException extends Exception
{
    public $message = 'GAME_ENDED';
}
