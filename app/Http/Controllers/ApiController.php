<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Wordle\Helpers\JsonPresenter;

class ApiController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected JsonPresenter $presenter;

    public function __construct()
    {
        $this->presenter = resolve(JsonPresenter::class);
    }
}
