<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\GameEndedException;
use App\Http\Controllers\ApiController;
use App\Http\Requests\MakeGuessRequest;
use App\Models\Game;
use Illuminate\Http\JsonResponse;
use Wordle\Game\Repositories\GameRepository;
use Wordle\Game\Repositories\GuessRepository;
use Wordle\Game\Transformers\GameTransformer;
use Wordle\Game\Transformers\GuessTransformer;

class GameController extends ApiController
{
    public function new(): JsonResponse
    {
        /** @var GameRepository $gameRepository */
        $gameRepository = resolve(GameRepository::class);
        $game = $gameRepository->create();

        return $this->presenter
            ->success([
                'game' => resolve(GameTransformer::class)->transform($game)
            ]);
    }

    public function read(Game $game): JsonResponse
    {
        return $this->presenter
            ->success([
                'game' => resolve(GameTransformer::class)->transform($game)
            ]);
    }

    /**
     * @throws GameEndedException
     */
    public function guess(MakeGuessRequest $request, Game $game): JsonResponse
    {
        /** @var GuessRepository $guessRepository */
        $guessRepository = resolve(GuessRepository::class);
        $guess = $guessRepository->create($game, $request->get('guess'));

        return $this->presenter->success([
            'guess' => resolve(GuessTransformer::class)->transform($guess)
        ]);
    }
}
