<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MakeGuessRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'guess' => 'required',
        ];
    }
}
