<?php

namespace App\Models;

use Database\Factories\GuessFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Wordle\Game\Actions\MatchGuessAction;
use Wordle\Game\Actions\ValidateGuessAction;
use Wordle\Game\Exceptions\IncompatibleStringSizesException;

/**
 * @property int $id
 * @property int $game_id
 * @property string $word
 * @property Game $game
 * @property boolean $correct
 * @property boolean $valid
 * @property string $match
 * @method static create(array $array)
 * @method static GuessFactory factory($count = null, $state = [])
 */
class Guess extends Model
{
    use HasFactory;

    public $fillable = [
        'game_id',
        'word'
    ];

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }

    public function getCorrectAttribute(): bool
    {
        return $this->word === $this->game->word->word;
    }

    public function getValidAttribute(): bool
    {
        return resolve(ValidateGuessAction::class)->execute($this->word);
    }

    public function getMatchAttribute(): ?string
    {
        try {
            return resolve(MatchGuessAction::class)->execute($this->game->word->word, $this->word);
        } catch (IncompatibleStringSizesException $e) {
            return null;
        }
    }
}
