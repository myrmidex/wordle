<?php

namespace App\Models;

use App\Traits\UUID;
use Database\Factories\GameFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $id
 * @property int $word_id
 * @property Word $word
 * @property bool $finished
 * @property bool $valid
 * @property bool $won
 * @method static GameFactory factory($count = null, $state = [])
 * @method static create(array $array)
 */
class Game extends Model
{
    use UUID;
    use HasFactory;

    public $fillable = [
        'word_id',
    ];

    public function word(): BelongsTo
    {
        return $this->belongsTo(Word::class);
    }

    public function guesses(): HasMany
    {
        return $this->hasMany(Guess::class);
    }

    public function getFinishedAttribute(): bool
    {
        if ($this->guesses()->where('word', $this->word->word)->count() > 0) {
            return true;
        }

        if ($this->guesses()->count() >= config('wordle.max_attempts')) {
            return true;
        }

        return false;
    }

    public function getWonAttribute(): bool
    {
        $wordGuessed = $this->guesses()->where('word', $this->word->word)->count() > 0;

        if ($wordGuessed && $this->guesses()->count() <= config('wordle.max_attempts')) {
            return true;
        }

        return false;
    }
}
