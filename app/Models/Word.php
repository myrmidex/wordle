<?php

namespace App\Models;

use Database\Factories\WordFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $word
 * @method static create(string[] $array)
 * @method static WordFactory factory($count = null, $state = [])
 * @method static inRandomOrder()
 */
class Word extends Model
{
    use HasFactory;

    public $fillable = [
        'word'
    ];

    public function word(): BelongsTo
    {
        return $this->belongsTo(Word::class);
    }
}
