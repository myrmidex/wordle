<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class BaseTransformer
{
    protected Model $model;

    abstract public function definition(Model $model): array;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function transform(): array
    {
        return $this->definition($this->model);
    }
}
