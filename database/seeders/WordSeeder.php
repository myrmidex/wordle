<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        resolve(\Wordle\Game\Actions\ImportWordsAction::class)->execute();
    }
}
