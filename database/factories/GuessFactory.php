<?php

namespace Database\Factories;

use App\Models\Game;
use App\Models\Guess;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Guess>
 */
class GuessFactory extends Factory
{
    public function definition(): array
    {
        return [
            'game_id' => null,
            'word' => fake()->word,
        ];
    }

    public function game(Game $game): self
    {
        return $this->state(fn () => [
            'game_id' => $game->id,
        ]);
    }
}
