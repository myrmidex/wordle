<?php

namespace Database\Factories;

use App\Models\Game;
use App\Models\Word;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Game>
 */
class GameFactory extends Factory
{
    public function definition(): array
    {
        return [
            'word_id' => null,
        ];
    }

    public function word(Word $word): self
    {
        return $this->state(fn () => [
            'word_id' => $word->id,
        ]);
    }
}
